FROM registry.git.beagleboard.org/jkridner/debian-build:bookworm

WORKDIR /opt

RUN apt-get update && apt-get install -y \
    autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libgl-dev \
    libpcre3-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    lzip \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    python3 \
    python3-distutils \
    python3-mako \
    python3-packaging \
    python3-pkg-resources \
    python-is-python3 \
    ruby \
    sed \
    sqlite3 \
    unzip \
    wget \
    xz-utils \
    && apt-get clean

RUN git clone --branch build-2022-04-09 --depth 1 https://github.com/mxe/mxe.git
WORKDIR /opt/mxe
RUN make MXE_TARGETS=i686-w64-mingw32.static qtbase
RUN make MXE_TARGETS=i686-w64-mingw32.static qt5
ENV PATH="${PATH}:/opt/mxe/usr/bin"
RUN echo "export PATH=$PATH" > /etc/environment
RUN make MXE_TARGETS=i686-w64-mingw32.static curl
